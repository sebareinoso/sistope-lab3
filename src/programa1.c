#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int *vector, i, nrandom;

	vector = (int*)malloc(sizeof(int)*536870912);

	srand(time(NULL));

	for (i = 0; i < 536870912; i++)
	{
		nrandom = rand() % 536870912;
		vector[i] = nrandom;
	}

	return 0;
}