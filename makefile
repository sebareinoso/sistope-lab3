CC = gcc

EXEC1 = lab3-sec
EXEC2 = lab3-ran

ODIR = obj
SRC = src

_OBJ1 = programa1.o
OBJ1 = $(ODIR)/$(_OBJ1)

_OBJ2 = programa2.o
OBJ2 = $(ODIR)/$(_OBJ2)

all: $(EXEC1) $(EXEC2)

$(ODIR)/%.o: $(SRC)/%.c
	$(CC) -c -o $@ $^

$(EXEC1): $(OBJ1)
	$(CC) -o $@ $^

$(EXEC2): $(OBJ2)
	$(CC) -o $@ $^

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o $(EXEC1) $(EXEC2)